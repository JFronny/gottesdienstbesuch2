using System.Text.Json.Serialization;
using Gottesdienstbesuch2.Data;

namespace Gottesdienstbesuch2.Branding
{
    public class Brand : AnonymizedBrand
    {
        private string _passwordAdminHash;
        private string _passwordViewerHash;

        [JsonPropertyName("passwordAdminHash")]
        public string PasswordAdminHash
        {
            get => _passwordAdminHash ?? Program.Hash("admin");
            set => _passwordAdminHash = value;
        }

        [JsonPropertyName("passwordViewerHash")]
        public string PasswordViewerHash
        {
            get => _passwordViewerHash ?? Program.Hash("viewer");
            set => _passwordViewerHash = value;
        }

        public AnonymizedBrand Anonymize() =>
            new()
            {
                Branding = Branding,
                Online = Online,
                MinTime = MinTime ?? "-eine"
            };
    }
}