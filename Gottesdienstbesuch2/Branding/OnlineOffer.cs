using System;
using System.Text.Json.Serialization;

namespace Gottesdienstbesuch2.Branding
{
    public class OnlineOffer
    {
        [JsonPropertyName("enabled")] public bool Enabled { get; set; }
        [JsonPropertyName("name")] public string Name { get; set; }
        [JsonPropertyName("url")] public Uri Url { get; set; }
        [JsonPropertyName("logo")] public string Logo { get; set; }
    }
}