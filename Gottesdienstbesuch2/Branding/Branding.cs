using System;
using System.Text.Json.Serialization;

namespace Gottesdienstbesuch2.Branding
{
    public class Branding
    {
        [JsonPropertyName("logo")] public string Logo { get; set; }
        [JsonPropertyName("contacs")] public string Contacs { get; set; }
        [JsonPropertyName("dataProcessingInfo")] public Uri DataProcessingInfo { get; set; }
    }
}