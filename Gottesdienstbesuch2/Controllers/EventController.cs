using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Gottesdienstbesuch2.Data;
using Microsoft.AspNetCore.Mvc;
using static Gottesdienstbesuch2.Database.Databases;

namespace Gottesdienstbesuch2.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EventController : ControllerBase
    {
        [HttpPut(Name = nameof(AddEvent))]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        [ProducesResponseType(401, Type = typeof(JfException))]
        [ProducesResponseType(409, Type = typeof(JfException))]
        public ActionResult<Guid> AddEvent(string brand, string name, string place, DateTime time, int maxParticipants, [FromBody] string password)
        {
            if (GetPrivLevel(password, brand) != PrivLevel.Admin)
                return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
            if (!Events.ContainsKey(brand))
                return BadRequest(new JfException("Unbekannte Brand", "Der angegebene Name ist unbeakannt"));
            if (Events[brand].Any(s => s.Value.Time == time && s.Value.Place == place && s.Value.Name == name))
                return Conflict(new JfException("Bereits vorhanden", "Ein äquivalentes Ereignis ist bereits eingetragen"));
            Guid g = Guid.NewGuid();
            Events[brand].Add(g, new Event
            {
                Active = true,
                MaxParticipants = maxParticipants,
                Name = name,
                Place = place,
                Time = time
            });
            return Ok(g);
        }

        [HttpGet("maxParticipants", Name = nameof(GetMaxParticipants))]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        public ActionResult<int> GetMaxParticipants(Guid eventId, bool ignoreOffset = false)
        {
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            int c = Events[eventId].MaxParticipants;
            if (ignoreOffset)
                c -= Events[eventId].MaxParticipantsOffset;
            return c;
        }

        [HttpGet(Name = nameof(GetEvents))]
        [ProducesResponseType(200, Type = typeof(IDictionary<string, AnonymizedEvent>))]
        public ActionResult<IDictionary<string, AnonymizedEvent>> GetEvents(string brand) => Ok(Events[brand].GetSendable(brand));

        [HttpDelete(Name = nameof(RemoveEvent))]
        [ProducesResponseType(200)]
        [ProducesResponseType(401, Type = typeof(JfException))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        public ActionResult RemoveEvent(Guid eventId, [FromBody] string password)
        {
            if (GetPrivLevel(password, Events.GetEventByGuid(eventId).Key) != PrivLevel.Admin)
                return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            Events.Remove(eventId);
            return Ok();
        }

        [HttpPatch(Name = nameof(ModifyEvent))]
        [ProducesResponseType(200)]
        [ProducesResponseType(401, Type = typeof(JfException))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        public ActionResult ModifyEvent(Guid eventId, [FromBody] string password, string name = null, string place = null, DateTime? time = null, int? maxParticipants = null, bool? active = null)
        {
            if (GetPrivLevel(password, Events.GetEventByGuid(eventId).Key) != PrivLevel.Admin)
                return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            Events.MutateEvent(eventId, s =>
            {
                s.Name = name ?? s.Name;
                s.Place = place ?? s.Place;
                s.Time = time ?? s.Time;
                s.MaxParticipants = maxParticipants ?? s.MaxParticipants;
                s.Active = active ?? s.Active;
            });
            return Ok();
        }
    }
}