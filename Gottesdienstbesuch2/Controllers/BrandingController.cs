using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using CC_Functions.AspNet;
using Gottesdienstbesuch2.Branding;
using Gottesdienstbesuch2.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using static Gottesdienstbesuch2.Database.Databases;

namespace Gottesdienstbesuch2.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BrandingController : ControllerBase
    {
        public static readonly string BrandDir = Path.Combine(BaseDir, "branding");
        public static readonly string BrandsFile = Path.Combine(BrandDir, "brands.json");
        
        [HttpGet(Name = nameof(GetBrands))]
        [ProducesResponseType(200, Type = typeof(IDictionary<string, AnonymizedBrand>))]
        public ActionResult<IDictionary<string, AnonymizedBrand>> GetBrands() => Ok(GetBrandsA().ToDictionary(s => s.Key, s => s.Value.Anonymize()));

        public static IDictionary<string, Brand> GetBrandsA() =>
            JsonSerializer.Deserialize<Dictionary<string, Brand>>(
                System.IO.File.ReadAllText(BrandsFile),
                new JsonSerializerOptions().AddCcf());

        [HttpGet("resource", Name = nameof(GetResource))]
        [ProducesResponseType(200)]
        [ProducesResponseType(404, Type = typeof(JfException))]
        public ActionResult GetResource(string resource)
        {
            string[] resources = Directory.GetFiles(BrandDir)
                .Select(Path.GetFullPath)
                .Where(s => s != BrandsFile).ToArray();
            if (resources.All(s => Path.GetFileName(s) != resource))
                return NotFound(new JfException("Nicht gefunden", "Die angeforderte Ressource wurde nicht gefunden"));
            string fileName = resources.First(s => Path.GetFileName(s) == resource);
            return new PhysicalFileResult(fileName, new FileExtensionContentTypeProvider().TryGetContentType(fileName, out string t) ? t : "application/octet-stream");
        }
    }
}