using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Gottesdienstbesuch2.Data;
using Gottesdienstbesuch2.Database;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Compatibility;
using OfficeOpenXml.Style;
using static Gottesdienstbesuch2.Database.Databases;

namespace Gottesdienstbesuch2.Controllers
{
    [ApiController]
    [Route("api")]
    public class MiscellaneousController : ControllerBase
    {
        private readonly Regex _alphanum = new("[^a-zA-Z0-9 -]");
        [HttpPut("privLevel", Name = nameof(GetPrivLevel))]
        [ProducesResponseType(200, Type = typeof(PrivLevel))]
        public ActionResult<PrivLevel> GetPrivLevel([FromBody] string password, string brand) => Ok(Databases.GetPrivLevel(password, brand));

        [HttpGet("eventTable.xlsx", Name = nameof(DownloadEventTable))]
        [ProducesResponseType(200)]
        [ProducesResponseType(400, Type = typeof(JfException))]
        [ProducesResponseType(401, Type = typeof(JfException))]
        public ActionResult DownloadEventTable(Guid eventId, string password)
        {
            if (Databases.GetPrivLevel(password, Events.GetEventByGuid(eventId).Key) == PrivLevel.Basic)
                return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            Event @event = Events[eventId];
            using ExcelPackage package = new();
            ExcelWorksheet ws = package.Workbook.Worksheets.Add(_alphanum.Replace(@event.Name, ""));
            int row = 1;
            //Info header
            FormatCell(ws, row, 1, "Datum:", true);
            FormatCell(ws, row, 2, @event.Time.ToString("g", new CultureInfo("de")) + " Uhr");
            row++;
            FormatCell(ws, row, 1, "Ereignis:", true);
            FormatCell(ws, row, 2, @event.Name);
            row++;
            FormatCell(ws, row, 1, "Ort:", true);
            FormatCell(ws, row, 2, @event.Place);
            FormatCell(ws, row, 1, "Teilnehmer:", true);
            FormatCell(ws, row, 2, @event.MaxParticipants.ToString());
            //Sorter
            row++;
            FormatCell(ws, row, 1, "Ordner 1:", true);
            FormatCell(ws, row, 3, "Ordner 2:", true);
            //Title row
            row += 2;
            FormatCell(ws, row, 1, "No.", true, true, true);
            FormatCell(ws, row, 2, "Nachname", true, true, true);
            FormatCell(ws, row, 3, "Vorname", true, true, true);
            FormatCell(ws, row, 4, "Telefon", true, true, true);
            FormatCell(ws, row, 5, "Platz", true, true, true);
            //Main table
            int i = 0;
            using Dictionary<Guid, Participation>.ValueCollection.Enumerator p = @event.Participants.Values.GetEnumerator();
            while (i < @event.MaxParticipants)
            {
                row++;
                FormatCell(ws, row, 2, table:true);
                FormatCell(ws, row, 3, table:true);
                FormatCell(ws, row, 4, table:true);
                FormatCell(ws, row, 5, table:true);
                if (p.MoveNext())
                {
                    FormatCell(ws, row, 2, p.Current.Name.Split(',')[0].TrimStart(), table:true);
                    FormatCell(ws, row, 3, p.Current.Name.Split(',')[1].TrimStart(), table:true);
                    FormatCell(ws, row, 4, p.Current.PhoneNumber, table:true);
                    i += p.Current.Count;
                    FormatCell(ws, row, 1, p.Current.Count > 1 ? $"{i - p.Current.Count + 1}-{i} ({p.Current.Count})" : i.ToString(), table: true);
                }
                else
                {
                    i++;
                    FormatCell(ws, row, 1, i.ToString(), table:true);
                }
            }
            for (i = 1; i < 5; i++)
                ws.Column(i).AutoFit();
            //Yay
            return new FileContentResult(package.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }

        private void FormatCell(ExcelWorksheet ws, int row, int column, string text = null, bool bold = false, bool table = false, bool header = false)
        {
            if (text != null)
            {
                ws.Cells[row, column].Value = text;
                ws.Cells[row, column].Style.Font.Name = "Helvetica";
            }
            if (bold) ws.Cells[row, column].Style.Font.Bold = true;
            if (table) ws.Cells[row, column].Style.Border.BorderAround(ExcelBorderStyle.Thin);
            if (header) ws.Cells[row, column].Style.Fill.SetBackground(Color.Khaki);
        }
    }
}
