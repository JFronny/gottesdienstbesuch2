﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gottesdienstbesuch2.Data;
using Gottesdienstbesuch2.Database;
using Microsoft.AspNetCore.Mvc;
using static Gottesdienstbesuch2.Database.Databases;

namespace Gottesdienstbesuch2.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParticipantController : ControllerBase
    {
        [HttpPut(Name = nameof(AddParticipant))]
        [ProducesResponseType(200, Type = typeof(Guid))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        public ActionResult<Guid> AddParticipant(Guid eventId, string name, string phoneNumber, int userCount)
        {
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(phoneNumber))
                return BadRequest(new JfException("Falsche Eingabe", "Die angegebenen Daten sind unvollständig"));
            if (name.Split(",").Length != 2)
                return BadRequest(new JfException("Falsche Eingabe", "Der Name ist falsch angegeben"));
            if (name.Split(",")[0].Trim().Length == 0)
                return BadRequest(new JfException("Falsche Eingabe", "Kein Nachname angegeben"));
            if (name.Split(",")[1].Trim().Length == 0)
                return BadRequest(new JfException("Falsche Eingabe", "Kein Vorname angegeben"));
            if (name.Length > 100)
                return BadRequest(new JfException("Name zu lang", "Der angegebene Name ist zu lang"));
            if (phoneNumber.Length > 15)
                return BadRequest(new JfException("Telefonnummer zu lang", "Die angegebene Telefonnummer ist zu lang"));
            Guid p = Guid.NewGuid();
            switch (Events.MutateEventE(eventId, s =>
            {
                if (!s.Active)
                    return 1;
                if (userCount < 1)
                    return 2;
                if (s.ParticipantCount + userCount > s.MaxParticipants)
                    return 3;
                if (s.Participants.Any(t => t.Value.Name == name && t.Value.PhoneNumber == phoneNumber))
                {
                    KeyValuePair<Guid, Participation> q = s.Participants.First(r => r.Value.Name == name && r.Value.PhoneNumber == phoneNumber);
                    q.Value.Count += userCount;
                    p = q.Key;
                }
                else
                    s.Participants.Add(p, new Participation(name, phoneNumber, userCount));
                return 0;
            }))
            {
                default:
                    return Ok(p);
                case 1:
                    return BadRequest(new JfException("Ereignis inaktiv", "Das Ereignis ist nicht mehr aktiv"));
                case 2:
                    return BadRequest(new JfException("Zu wenig Teilnehmer",
                        "Zu wenige Teilnehmer (<1) wurden angegeben"));
                case 3:
                    return BadRequest(new JfException("Zu viele Teilnehmer",
                        "Es haben sich bereits zu viele eingetragen"));
            }
        }

        [HttpPut("offsetCount", Name = nameof(ModifyParticipantsCount))]
        [ProducesResponseType(200)]
        [ProducesResponseType(401, Type = typeof(JfException))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        public ActionResult ModifyParticipantsCount(Guid eventId, Guid participationId, int countOffset, [FromBody] string password)
        {
            if (GetPrivLevel(password, Events.GetEventByGuid(eventId).Key) != PrivLevel.Admin)
                return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            switch (Events.MutateEventE(eventId, s =>
            {
                if (!s.Participants.ContainsKey(participationId))
                    return 1;
                if (s.Participants[participationId].Count + countOffset <= 0)
                    s.Participants.Remove(participationId);
                else
                    s.Participants[participationId].Count += countOffset;
                if (countOffset > 0 && s.ParticipantCount > s.MaxParticipants)
                {
                    s.Participants[participationId].Count -= countOffset;
                    return 2;
                }
                return 0;
            }))
            {
                default:
                    return Ok();
                case 1:
                    return BadRequest(new JfException("Unbekannter Eintrag", "Die ID des Eintrags ist inkorrekt"));
                case 2:
                    return BadRequest(new JfException("Zu viele Teilnehmer", "Es haben sich bereits zu viele eingetragen"));
            }
        }

        [HttpDelete(Name = nameof(ClearParticipants))]
        [ProducesResponseType(200)]
        [ProducesResponseType(401, Type = typeof(JfException))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        public ActionResult ClearParticipants(Guid eventId, [FromBody] string password)
        {
            if (GetPrivLevel(password, Events.GetEventByGuid(eventId).Key) != PrivLevel.Admin)
                return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            Events.MutateEvent(eventId, s => s.Participants.Clear());
            return Ok();
        }

        [HttpGet("count", Name = nameof(CountParticipants))]
        [ProducesResponseType(200, Type = typeof(int))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        public ActionResult<int> CountParticipants(Guid eventId)
        {
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            return Ok(Events[eventId].ParticipantCount);
        }

        [HttpGet(Name = nameof(GetParticipants))]
        [ProducesResponseType(200, Type = typeof(IDictionary<Guid, Participation>))]
        [ProducesResponseType(401, Type = typeof(JfException))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        public ActionResult<IDictionary<Guid, Participation>> GetParticipants(Guid eventId, string password) //TODO because this is a GET request it can't have a body
        {
            if (GetPrivLevel(password, Events.GetEventByGuid(eventId).Key) < PrivLevel.Viewer)
                return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            return Ok(Events[eventId].Participants);
        }

        [HttpPatch(Name = nameof(RemoveParticipation))]
        [ProducesResponseType(200)]
        [ProducesResponseType(401, Type = typeof(JfException))]
        [ProducesResponseType(400, Type = typeof(JfException))]
        public ActionResult RemoveParticipation(Guid eventId, Guid participationId, [FromBody] string password)
        {
            if (GetPrivLevel(password, Events.GetEventByGuid(eventId).Key) != PrivLevel.Admin)
                return Unauthorized(new JfException("Unauthorisiert", "Nur Administratoren haben hierauf Zugriff"));
            if (!Events.ContainsKey(eventId))
                return BadRequest(new JfException("Unbekanntes Ereignis", "Die ID des Ereignises ist inkorrekt"));
            if (Events.MutateEventE(eventId, s =>
            {
                if (s.Participants.ContainsKey(participationId))
                    s.Participants.Remove(participationId);
                else
                    return 1;
                return 0;
            }) == 1)
                return BadRequest(new JfException("Unbekannter Eintrag", "Die ID des Eintrags ist inkorrekt"));
            return Ok();
        }
    }
}