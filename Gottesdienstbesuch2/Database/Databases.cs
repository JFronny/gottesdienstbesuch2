using System.IO;
using System.Reflection;
using Gottesdienstbesuch2.Branding;
using Gottesdienstbesuch2.Controllers;
using Gottesdienstbesuch2.Data;

namespace Gottesdienstbesuch2.Database
{
    public class Databases
    {
        public static readonly string BaseDir = Path.GetFullPath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
        public static readonly string Dir = Path.Combine(BaseDir, "DB");

        public const string Extension = "db";
        
        public static EventsD Events = new();

        public static PrivLevel GetPrivLevel(string password, string brand)
        {
            Brand b = BrandingController.GetBrandsA()[brand ?? "default"];
            b.PasswordAdminHash = b.PasswordAdminHash.ToLower();
            b.PasswordViewerHash = b.PasswordViewerHash.ToLower();
            string hash = Program.Hash(password).ToLower();
            if (hash == b.PasswordAdminHash)
                return PrivLevel.Admin;
            if (hash == b.PasswordViewerHash)
                return PrivLevel.Viewer;
            return PrivLevel.Basic;
        }
    }
}