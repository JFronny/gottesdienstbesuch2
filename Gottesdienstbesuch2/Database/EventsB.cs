using System.Collections.Generic;
using System.Linq;
using CC_Functions.AspNet;
using Gottesdienstbesuch2.Data;

namespace Gottesdienstbesuch2.Database
{
    public class EventsB : SerialDict<Event>
    {
        private readonly string _brand;

        public EventsB(string brand) => _brand = brand;

        public IDictionary<string, AnonymizedEvent> GetSendable(string brand)
        {
            lock (this)
            {
                Dictionary<string, AnonymizedEvent> result = new();
                foreach (KeyValuePair<string, Event> kvp in base.GetSendable()
                    .OrderBy(s => s.Value.Time))
                {
                    result.Add(kvp.Key, new AnonymizedEvent
                    {
                        Active = kvp.Value.Active,
                        MaxParticipants = kvp.Value.MaxParticipants,
                        Name = kvp.Value.Name,
                        Place = kvp.Value.Place,
                        Time = kvp.Value.Time,
                        ParticipantCount = kvp.Value.ParticipantCount
                    });
                }
                return result;
            }
        }

        public override string DatabasesDir => Databases.Dir;
        public override string DatabaseFileName => $"{_brand}.{Databases.Extension}";
    }
}