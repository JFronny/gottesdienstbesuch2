using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CC_Functions.AspNet;
using Gottesdienstbesuch2.Branding;
using Gottesdienstbesuch2.Controllers;
using Gottesdienstbesuch2.Data;

namespace Gottesdienstbesuch2.Database
{
    public class EventsD : SaveLoadDict<string, EventsB>
    {
        protected override void Save(IDictionary<string, EventsB> v)
        {
        }

        protected override IDictionary<string, EventsB> Load()
        {
            Dictionary<string, EventsB> v = new();
            foreach (string file in Directory.GetFiles(Databases.Dir))
                if (file.EndsWith($".{Databases.Extension}"))
                    v.Add(Path.GetFileNameWithoutExtension(file), new EventsB(Path.GetFileNameWithoutExtension(file)));
            foreach (KeyValuePair<string, Brand> kvp in BrandingController.GetBrandsA())
                if (!v.ContainsKey(kvp.Key))
                    v.Add(kvp.Key, new EventsB(kvp.Key));
            return v;
        }
        
        public bool ContainsKey(Guid v)
        {
            return Values.Any(s => s.ContainsKey(v));
        }

        public Event this[Guid k]
        {
            get => GetEventByGuid(k).Value[k];
            set => GetEventByGuid(k).Value[k] = value;
        }

        public void Remove(Guid k) => GetEventByGuid(k).Value.Remove(k);

        public KeyValuePair<string, EventsB> GetEventByGuid(Guid k) => GetEventByGuid(k, this);

        public KeyValuePair<string, EventsB> GetEventByGuid(Guid k, IDictionary<string, EventsB> d)
        {
            return d.First(s => s.Value.ContainsKey(k));
        }

        public void MutateEvent(Guid k, Action<Event> f)
        {
            Mutate(s =>
            {
                string d = GetEventByGuid(k, s).Key;
                Event e = s[d][k];
                f(e);
                s[d][k] = e;
                return s;
            });
        }

        public int MutateEventE(Guid k, Func<Event, int> f)
        {
            int status = 0;
            Mutate(s =>
            {
                string d = GetEventByGuid(k, s).Key;
                Event e = s[d][k];
                status = f(e);
                s[d][k] = e;
                return s;
            });
            return status;
        }
    }
}