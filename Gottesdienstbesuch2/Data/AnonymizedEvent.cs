using System;
using System.Collections.Generic;

namespace Gottesdienstbesuch2.Data
{
    public class AnonymizedEvent
    {
        public DateTime Time { get; set; }
        public string Place { get; set; }
        public int MaxParticipants { get; set; }
        public int ParticipantCount { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
    }
}