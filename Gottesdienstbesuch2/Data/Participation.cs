using System;
using ProtoBuf;

namespace Gottesdienstbesuch2.Data
{
    [ProtoContract]
    public class Participation
    {
        public Participation(string name, string phoneNumber, int count)
        {
            Name = name;
            PhoneNumber = phoneNumber;
            Count = count;
            EntryTime = DateTime.Now;
        }

        public Participation()
        {
            
        }

        [ProtoMember(1)] public string Name { get; set; }
        [ProtoMember(2)] public string PhoneNumber { get; set; }
        [ProtoMember(3)] public int Count { get; set; }
        [ProtoMember(4)] public DateTime EntryTime { get; set; }
    }
}