using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using ProtoBuf;

namespace Gottesdienstbesuch2.Data
{
    [ProtoContract]
    public class Event
    {
        [ProtoMember(1)] public DateTime Time { get; set; }
        [ProtoMember(2)] public string Place { get; set; }

        [ProtoMember(3)] private int _maxParticipants;
        public int MaxParticipants
        {
            get => _maxParticipants + MaxParticipantsOffset;
            set => _maxParticipants = value - MaxParticipantsOffset;
        }

        [ProtoMember(4)] public string Name { get; set; }
        [ProtoMember(5)] public bool Active { get; set; }

        [ProtoMember(6)] private Dictionary<Guid, Participation> _participants;

        public Dictionary<Guid, Participation> Participants
        {
            get
            {
                _participants ??= new Dictionary<Guid, Participation>();
                _participants = _participants
                    .OrderBy(s => s.Value.Name)
                    .ToDictionary(s => s.Key, s => s.Value);
                return _participants;
            }
            set => _participants = value ?? _participants ?? new Dictionary<Guid, Participation>();
        }

        public int ParticipantCount => Participants.Aggregate(0, (i, participation) => i + participation.Value.Count);

        [JsonIgnore]
        public int MaxParticipantsOffset
        {
            get
            {
                return Participants.Aggregate(0, (s, i) =>
                {
                    if (DateTime.Now - i.Value.EntryTime > new TimeSpan(0, 2, 0))
                        return s + i.Value.Count / 2;
                    else
                        return s;
                });
            }
        }
    }
}