using System.Text.Json.Serialization;
using Gottesdienstbesuch2.Branding;

namespace Gottesdienstbesuch2.Data
{
    public class AnonymizedBrand
    {
        [JsonPropertyName("branding")] public Branding.Branding Branding { get; set; }
        [JsonPropertyName("online")] public OnlineOffer Online { get; set; }
        [JsonPropertyName("minTime")] public string MinTime { get; set; }
    }
}