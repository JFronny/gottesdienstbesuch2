namespace Gottesdienstbesuch2.Data
{
    public enum PrivLevel
    {
        Basic,
        Viewer,
        Admin
    }
}