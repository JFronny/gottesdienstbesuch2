using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CC_Functions.AspNet;
using CC_Functions.Commandline;
using Gottesdienstbesuch2.Branding;
using Gottesdienstbesuch2.Controllers;
using Gottesdienstbesuch2.Database;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using OfficeOpenXml;

namespace Gottesdienstbesuch2
{
    public static class Program
    {
        public static async Task Main(string[] a)
        {
            ArgsParse args = new(a);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            bool printdb = args.GetBool("printdb");
            bool hash = args["hash"] != null;
            bool open = args.GetBool("open") || !printdb && !hash;
            if (hash) Console.WriteLine(Hash(args["hash"]));
            if (printdb)
                Console.WriteLine(JsonSerializer.Serialize(Databases.Events,
                    new JsonSerializerOptions().AddCcf()));
            if (open)
            {
                if (!Directory.Exists(BrandingController.BrandDir))
                    Directory.CreateDirectory(BrandingController.BrandDir);
                if (!File.Exists(BrandingController.BrandsFile))
                {
                    Dictionary<string, Brand> defaultData = new Dictionary<string, Brand>
                    {
                        {
                            "default", new Brand
                            {
                                Branding = new Branding.Branding
                                {
                                    Contacs =
                                        "Katholische Seelsorgeeinheit Mittleres Heckengäu, <br />Pfr. Anton Gruber, <br />Kapuzinerberg 1, 71263 Weil der Stadt, <br />Telefon: 07033 52683, <br />info@kirchewds.de",
                                    Logo = "mittleres-heckengäu.png",
                                    DataProcessingInfo = new Uri("https://www.mh-drs.de/meta/datenschutz/")
                                },
                                Online = new OnlineOffer
                                {
                                    Enabled = true,
                                    Logo = "ngl.png",
                                    Name = "Netzgottesdienst",
                                    Url = new Uri("http://www.netzgottesdienst.de")
                                },
                                PasswordAdminHash = Hash("admin"),
                                PasswordViewerHash = Hash("viewer")
                            }
                        }
                    };
                    await File.WriteAllTextAsync(BrandingController.BrandsFile, JsonSerializer.Serialize(defaultData, new JsonSerializerOptions().AddCcf()));
                }
                string defF = Path.Combine(Databases.Dir, $"default.{Databases.Extension}");
                string evF = Path.Combine(Databases.Dir, $"events.{Databases.Extension}");
                if (File.Exists(evF) && !File.Exists(defF))
                    File.Move(evF, defF);
                await CreateHostBuilder(a).Build().RunAsync();
            }
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });

        public static string Hash(string text)
        {
            text ??= "";
            using MD5 sec = new MD5CryptoServiceProvider();
            Encoding utf8 = Encoding.UTF8;
            return BitConverter.ToString(sec.ComputeHash(utf8.GetBytes(text)));
        }
    }
}